class BattleshipGame
  attr_reader :player_one, :player_two, :current_player, :current_attack_board, :current_defend_board, :attack_board_one, :attack_board_two, :defend_board_one, :defend_board_two

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @current_player = player_one

  end

  def create_boards
    @attack_board_one = Board.new
    @attack_board_two = Board.new
    @defend_board_one = Board.new
    @defend_board_two = Board.new
    @current_attack_board = @attack_board_one
    @current_defend_board = @defend_board_two
    @previous_defend_board = @defend_board_one
  end

  #attack is fed a position in the form of [row,col], then marks current boards
  def attack(position)
    target = @current_defend_board.grid[position[0]][position[1]]
    if target == :s
      @current_attack_board.grid[position[0]][position[1]] = :h
    else
      @current_attack_board.grid[position[0]][position[1]] = :m
    end
    @current_defend_board.grid[position[0]][position[1]] = :x
  end

  #takes in a player object and places a ship on the board
  def place_ship
    #raise "error" if Board.full?
    positions = @current_player.set_up
    #[[0,0],[0,3]] ship of length 4

    if positions[0][0] == positions[1][0]
      #ship is horizontal
      row = positions[0][0]
      start_col = [positions[0][1],positions[1][1]].min
      end_col = [positions[0][1],positions[1][1]].max
      (start_col..end_col).each do |col|
        @previous_defend_board.grid[row][col] = :s
      end
    else
      #ship is vertical
      #[[0,0],[3,0]]
      column = positions[0][1]
      start_row = [positions[0][0],positions[1][0]].min
      end_row = [positions[0][0],positions[1][0]].max
      (start_row..end_row).each do |rows|
        @previous_defend_board.grid[rows][column] = :s
      end
    end
  end

  def count(board)
    board.count
  end


  def game_over?(board)
    board.won?
    #current_player wins
  end

  def play_turn
    puts "Attacking board:"
    @current_player.display(@current_attack_board)
    puts ""
    puts "Defending board:"
    @current_player.display(@previous_defend_board)
    puts ""
    target = @current_player.get_play
    while @current_attack_board.grid[target[0]][target[1]] != nil
      target = @current_player.get_play
    end
    self.attack(target)
    switch_players
  end


  #modify play for computer and human player
  #
  def play
    create_boards
    #setup phase: Board.place_ship five times, for each player
    #need a current player and then a switch
    2.times do
      3.times do
        place_ship
      end
      switch_players
    end

    #1. if @current_player is human, display the computer board,
    until game_over?(@current_defend_board)

      #puts "There are #{self.count} targets left"
      self.play_turn
    end
    puts "Game Over"

  end

  def switch_players
    if @current_player == @player_one
      @current_player = @player_two
      @current_attack_board = @attack_board_two
      @current_defend_board = @defend_board_one
      @previous_defend_board = @defend_board_two
    else
      @current_player = @player_one
      @current_attack_board = @attack_board_one
      @current_defend_board = @defend_board_two
      @previous_defend_board = @defend_board_one
    end
  end


end
