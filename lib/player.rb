class HumanPlayer

  def display(board)
    print board.grid
  end

  def get_play
    puts "Please enter a position in the form of: row,column"
    position = $stdin.gets.chomp
    idx_of_comma = nil
    position.each_char.with_index do |char,index|
      idx_of_comma = index if char == ","
    end
    #assume boards will only be length 10 maximum, meaning indices 0-9
    #[position[0].to_i,position[2].to_i]
    [position[0...idx_of_comma].to_i,position[idx_of_comma+1..position.length-1].to_i]
  end

  #return array: [start position, end position]
  def set_up
    puts "Choose a ship: Aircraft carrier, Battleship, Submarine, Destroyer, Patrol boat"
    ship_name = $stdin.gets.chomp
    new_ship = Ship.new(ship_name)
    new_ship_size = new_ship.size
    puts "#{ship_name} is #{new_ship_size} units long.  Please enter a start position in the form of row,column"
    start_position = $stdin.gets.chomp
    puts "#{ship_name} is #{new_ship_size} units long.  Please enter an end position in the form of row,column"
    end_position = $stdin.gets.chomp
    [[start_position[0].to_i,start_position[2].to_i],[end_position[0].to_i,end_position[2].to_i]]
  end

end
#A HumanPlayer class, responsible for translating
#user input into positions of the form [x, y]


#5 ships of length 5,4,3,3,2, can be placed on board
#horizontally or vertically
#placing a ship: starting point, ending point
class Ship
  attr_reader :name, :size

  def initialize(name)
    @name = name
    if @name == "Aircraft carrier"
      @size = 5
    elsif @name == "Battleship"
      @size = 4
    elsif @name == "Submarine"
      @size = 3
    elsif @name == "Destroyer"
      @size = 3
    elsif @name == "Patrol boat"
      @size = 2
    end
  end

end


class ComputerPlayer
  attr_reader :board
  #get_play: random until hits something
  #more strategy to come
  def display(board)
    @board = board
  end

  def get_play
    row = rand(10)
    col = rand(10)
    [row,col]
  end

  def set_up
    rand_col = rand(10)
    [[0,rand_col],[3,rand_col]]
  end
end
