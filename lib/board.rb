class Board

  attr_reader :grid


  def initialize (grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  def count
    count = 0
    @grid.each do |el1|
      el1.each do |el2|
        count+=1 if el2 == :s
      end
    end
    count
  end

  def empty?(position=true)
    if position != true
      return true if @grid[position[0]][position[1]] == nil
      return false
    else
     @grid.each do |el1|
        el1.each do |el2|
          return false if el2 == :s
        end
     end
      return true
    end
  end

  def full?
    @grid.each do |el1|
       el1.each do |el2|
         return false if el2 == nil
       end
    end
     return true
  end



  def won?
    @grid.each do |el1|
       el1.each do |el2|
         return false if el2 == :s
       end
    end
     return true
  end


  def [](pos)
    @grid[pos.first][pos.last]
  end

#  def [](pos)
#    x, y = pos
#    @grid[x][y]
#   end
end
